use Test::More tests => 2;

use_ok(Vernier::GoTemp);

my $temp = Vernier::GoTemp->new(
    duration => '60s'
);

isa_ok($temp, 'Vernier::GoTemp');

$temp->start;


