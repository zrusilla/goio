use Test::More tests => 2;
use Vernier::GoIO qw(:basic);

my $name = get_available_device(GoTemp);

ok($name, 'get available device');

my $handle =  open_device($name, GoTemp);

ok($handle, 'open device');

close_device($handle);
