use Test::More tests => 5;
use Vernier::GoIO qw(:basic);

my $name = get_available_device(GoTemp);

ok($name, 'get available device');

my $handle =  open_device($name, GoTemp);

my $tick = get_tick($handle);

ok($tick, "tick $tick");

my $min = get_min_period($handle);
my $max = get_max_period($handle);

ok($min, $min);
ok($max, $max);

my $interval = set_measurement_interval($handle, '500ms');

diag $interval;

ok($handle, 'open device');

close_device($handle);
