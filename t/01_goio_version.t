use Test::More tests => 1;

use Vernier::GoIO qw(:basic);

my $version = get_GoIO_version;

like($version, qr{\d+\.\d+});

