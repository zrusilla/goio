use Test::More tests => 2;

use_ok(Vernier::GoTemp);

my $temp = Vernier::GoTemp->new;

isa_ok($temp, 'Vernier::GoTemp');

$temp->start;
sleep(1);

my @measurements = $temp->read_measurements;

map { diag "$_\n" } @measurements;

