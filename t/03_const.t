use strict;
use warnings;
use Test::More tests => 8;

use Vernier::GoIO qw(:const);

is (VernierID, 0x08F7);

is (LabPro, 1);
is (GoTemp, 2);
is (GoLink, 3);
is (GoMotion, 4);
is (LabQuest, 5);
is (CKSpectrometer, 6);
is (MiniGasChromatograph, 7);






