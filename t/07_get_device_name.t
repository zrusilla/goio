use Test::More qw(no_plan);

use Vernier::GoIO qw(:basic);

my $name = get_available_device(GoTemp);
ok($name, 'get available device');

my $handle =  open_device($name, GoTemp);
ok($handle, 'open device');

my %info = get_device_info($handle);


is($info{name},       $name,     "device name");
is($info{vendor_id},  VernierID, "vendor id");
is($info{product_id}, GoTemp,    "product id");

close_device($handle);
