# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl 00_use.t'

use Test::More tests => 2;
BEGIN { use_ok('Vernier::GoIO') };
BEGIN { use_ok('Vernier::GoTemp') };

