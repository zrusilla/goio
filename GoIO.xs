#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "include/GVernierUSB.h"
#include "include/GoIO_DLL_interface.h"

MODULE = Vernier::GoIO		PACKAGE = Vernier::GoIO		PREFIX = GoIO_

BOOT:
{
    HV *stash;
    stash = gv_stashpv("Vernier::GoIO", TRUE);
    newCONSTSUB(stash, "VernierID",            newSViv(VERNIER_DEFAULT_VENDOR_ID));
    newCONSTSUB(stash, "LabPro",               newSViv(LABPRO_DEFAULT_PRODUCT_ID));
    newCONSTSUB(stash, "GoTemp",               newSViv(USB_DIRECT_TEMP_DEFAULT_PRODUCT_ID));
    newCONSTSUB(stash, "GoLink",               newSViv(SKIP_DEFAULT_PRODUCT_ID));
    newCONSTSUB(stash, "GoMotion",             newSViv(CYCLOPS_DEFAULT_PRODUCT_ID));
    newCONSTSUB(stash, "LabQuest",             newSViv(NGI_DEFAULT_PRODUCT_ID));
    newCONSTSUB(stash, "CKSpectrometer",       newSViv(LOWCOST_SPEC_DEFAULT_PRODUCT_ID));
    newCONSTSUB(stash, "MiniGasChromatograph", newSViv(MINI_GC_DEFAULT_PRODUCT_ID));

    newCONSTSUB(stash, "StartMeasurements",    newSViv(SKIP_CMD_ID_START_MEASUREMENTS));
    newCONSTSUB(stash, "StopMeasurements",     newSViv(SKIP_CMD_ID_STOP_MEASUREMENTS));
    newCONSTSUB(stash, "DefaultTimeout",       newSViv(SKIP_TIMEOUT_MS_DEFAULT));
}

NO_OUTPUT gtype_int32
GoIO_GetDLLVersion(OUTLIST gtype_uint16 pMajorVersion, OUTLIST gtype_uint16 pMinorVersion)
        POSTCALL:
            if (RETVAL != 0)
            croak("Error %d while retrieving GoIO library version");

NO_OUTPUT gtype_int32
GoIO_GetNthAvailableDeviceName(gtype_int32 vendorId, gtype_int32 productId)
    PREINIT:
        char s[GOIO_MAX_SIZE_DEVICE_NAME];
        gtype_int32 found;
    PPCODE:
        found = GoIO_GetNthAvailableDeviceName(s, GOIO_MAX_SIZE_DEVICE_NAME, vendorId, productId, 0);
        if (found == 0) {
            EXTEND(SP, 1);
            PUSHs(sv_2mortal(newSVpv(s, strlen(s))));
        }
      
NO_OUTPUT gtype_int32
GoIO_Sensor_GetOpenDeviceName(GOIO_SENSOR_HANDLE hSensor)
    PREINIT:
        char name[GOIO_MAX_SIZE_DEVICE_NAME];
        gtype_int32 found;
        gtype_int32 vendorId;
        gtype_int32 productId;
    PPCODE:
        found = GoIO_Sensor_GetOpenDeviceName(hSensor, name, GOIO_MAX_SIZE_DEVICE_NAME, &vendorId, &productId);

        if (found == 0) {
            XPUSHs(sv_2mortal(newSVpv(name, strlen(name))));
            XPUSHs(sv_2mortal(newSViv(vendorId)));
            XPUSHs(sv_2mortal(newSViv(productId)));
        }


NO_OUTPUT gtype_int32
GoIO_Init()
        POSTCALL:
            if (RETVAL == -1)
            croak("Error %d: Unable to init GoIO device");


GOIO_SENSOR_HANDLE
GoIO_Sensor_Open(pDeviceName, vendorId, productId, strictDDSValidationFlag)
	const char *	pDeviceName
	gtype_int32	vendorId
	gtype_int32	productId
	gtype_int32	strictDDSValidationFlag


gtype_int32
GoIO_Sensor_SetMeasurementPeriod(GOIO_SENSOR_HANDLE hSensor, gtype_real64 desiredPeriod)
    PREINIT:
	gtype_int32  timeoutMs = SKIP_TIMEOUT_MS_DEFAULT;
    PPCODE:
        RETVAL = GoIO_Sensor_SetMeasurementPeriod(hSensor, desiredPeriod, timeoutMs);
            
gtype_real64
GoIO_Sensor_CalibrateData(hSensor, volts)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_real64	volts

gtype_int32
GoIO_Sensor_ClearIO(hSensor)
	GOIO_SENSOR_HANDLE	hSensor

gtype_int32
GoIO_Sensor_Close(hSensor)
	GOIO_SENSOR_HANDLE	hSensor

gtype_int32
GoIO_Sensor_GetNumMeasurementsAvailable(hSensor)
	GOIO_SENSOR_HANDLE	hSensor


NO_OUTPUT gtype_int32
GoIO_Sensor_ReadRawMeasurements(GOIO_SENSOR_HANDLE hSensor, gtype_int32 maxCount)
     PREINIT:
          gtype_int16 i = 0;
          gtype_int32 numMeasurements;
     PPCODE:
          gtype_int32 rawMeasurements[maxCount];
          numMeasurements =  GoIO_Sensor_ReadRawMeasurements(hSensor, rawMeasurements, maxCount);
          for (i = 0; i < numMeasurements; i++) { 
              XPUSHs(sv_2mortal(newSVnv(rawMeasurements[i])));
          }

gtype_real64
GoIO_Sensor_ConvertToVoltage(hSensor, rawMeasurement)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_int32	rawMeasurement

gtype_int32
GoIO_Sensor_DDSMem_CalculateChecksum(hSensor, pChecksum)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pChecksum

gtype_int32
GoIO_Sensor_DDSMem_GetActiveCalPage(hSensor, pActiveCalPage)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pActiveCalPage

gtype_int32
GoIO_Sensor_DDSMem_GetAveraging(hSensor, pAveraging)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pAveraging

gtype_int32
GoIO_Sensor_DDSMem_GetCalPage(hSensor, CalPageIndex, pCalibrationCoefficientA, pCalibrationCoefficientB, pCalibrationCoefficientC, pUnits, maxNumBytesToCopy)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	CalPageIndex
	gtype_real32 *	pCalibrationCoefficientA
	gtype_real32 *	pCalibrationCoefficientB
	gtype_real32 *	pCalibrationCoefficientC
	char *	pUnits
	gtype_uint16	maxNumBytesToCopy

gtype_int32
GoIO_Sensor_DDSMem_GetCalibrationEquation(hSensor, pCalibrationEquation)
	GOIO_SENSOR_HANDLE	hSensor
	char *	pCalibrationEquation

gtype_int32
GoIO_Sensor_DDSMem_GetChecksum(hSensor, pChecksum)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pChecksum

gtype_int32
GoIO_Sensor_DDSMem_GetCurrentRequirement(hSensor, pCurrentRequirement)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pCurrentRequirement

gtype_int32
GoIO_Sensor_DDSMem_GetExperimentType(hSensor, pExperimentType)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pExperimentType

gtype_int32
GoIO_Sensor_DDSMem_GetHighestValidCalPageIndex(hSensor, pHighestValidCalPageIndex)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pHighestValidCalPageIndex

gtype_int32
GoIO_Sensor_DDSMem_GetLongName(hSensor, pLongName, maxNumBytesToCopy)
	GOIO_SENSOR_HANDLE	hSensor
	char *	pLongName
	gtype_uint16	maxNumBytesToCopy

gtype_int32
GoIO_Sensor_DDSMem_GetLotCode(hSensor, pYY_BCD, pWW_BCD)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pYY_BCD
	unsigned char *	pWW_BCD

gtype_int32
GoIO_Sensor_DDSMem_GetManufacturerID(hSensor, pManufacturerID)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pManufacturerID

gtype_int32
GoIO_Sensor_DDSMem_GetMemMapVersion(hSensor, pMemMapVersion)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pMemMapVersion

gtype_int32
GoIO_Sensor_DDSMem_GetMinSamplePeriod(hSensor, pMinSamplePeriod)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_real32 *	pMinSamplePeriod

gtype_int32
GoIO_Sensor_DDSMem_GetOperationType(hSensor, pOperationType)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pOperationType

gtype_int32
GoIO_Sensor_DDSMem_GetRecord(hSensor, pRec)
	GOIO_SENSOR_HANDLE	hSensor
	GSensorDDSRec *	pRec

gtype_int32
GoIO_Sensor_DDSMem_GetSensorNumber(hSensor, pSensorNumber, sendQueryToHardwareflag, timeoutMs)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pSensorNumber
	gtype_int32	sendQueryToHardwareflag
	gtype_int32	timeoutMs

gtype_int32
GoIO_Sensor_DDSMem_GetSerialNumber(hSensor, pLeastSigByte_SerialNumber, pMidSigByte_SerialNumber, pMostSigByte_SerialNumber)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pLeastSigByte_SerialNumber
	unsigned char *	pMidSigByte_SerialNumber
	unsigned char *	pMostSigByte_SerialNumber

gtype_int32
GoIO_Sensor_DDSMem_GetShortName(hSensor, pShortName, maxNumBytesToCopy)
	GOIO_SENSOR_HANDLE	hSensor
	char *	pShortName
	gtype_uint16	maxNumBytesToCopy

gtype_int32
GoIO_Sensor_DDSMem_GetSignificantFigures(hSensor, pSignificantFigures)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pSignificantFigures

gtype_int32
GoIO_Sensor_DDSMem_GetTypNumberofSamples(hSensor, pTypNumberofSamples)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_uint16 *	pTypNumberofSamples

gtype_int32
GoIO_Sensor_DDSMem_GetTypSamplePeriod(hSensor, pTypSamplePeriod)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_real32 *	pTypSamplePeriod

gtype_int32
GoIO_Sensor_DDSMem_GetUncertainty(hSensor, pUncertainty)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pUncertainty

gtype_int32
GoIO_Sensor_DDSMem_GetWarmUpTime(hSensor, pWarmUpTime)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_uint16 *	pWarmUpTime

gtype_int32
GoIO_Sensor_DDSMem_GetYmaxValue(hSensor, pYmaxValue)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_real32 *	pYmaxValue

gtype_int32
GoIO_Sensor_DDSMem_GetYminValue(hSensor, pYminValue)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_real32 *	pYminValue

gtype_int32
GoIO_Sensor_DDSMem_GetYscale(hSensor, pYscale)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char *	pYscale

gtype_int32
GoIO_Sensor_DDSMem_ReadRecord(hSensor, strictDDSValidationFlag, timeoutMs)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_int32	strictDDSValidationFlag
	gtype_int32	timeoutMs

gtype_int32
GoIO_Sensor_DDSMem_SetActiveCalPage(hSensor, ActiveCalPage)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	ActiveCalPage

gtype_int32
GoIO_Sensor_DDSMem_SetAveraging(hSensor, Averaging)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	Averaging

gtype_int32
GoIO_Sensor_DDSMem_SetCalPage(hSensor, CalPageIndex, CalibrationCoefficientA, CalibrationCoefficientB, CalibrationCoefficientC, pUnits)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	CalPageIndex
	gtype_real32	CalibrationCoefficientA
	gtype_real32	CalibrationCoefficientB
	gtype_real32	CalibrationCoefficientC
	const char *	pUnits

gtype_int32
GoIO_Sensor_DDSMem_SetCalibrationEquation(hSensor, CalibrationEquation)
	GOIO_SENSOR_HANDLE	hSensor
	char	CalibrationEquation

gtype_int32
GoIO_Sensor_DDSMem_SetChecksum(hSensor, Checksum)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	Checksum

gtype_int32
GoIO_Sensor_DDSMem_SetCurrentRequirement(hSensor, CurrentRequirement)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	CurrentRequirement

gtype_int32
GoIO_Sensor_DDSMem_SetExperimentType(hSensor, ExperimentType)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	ExperimentType

gtype_int32
GoIO_Sensor_DDSMem_SetHighestValidCalPageIndex(hSensor, HighestValidCalPageIndex)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	HighestValidCalPageIndex

gtype_int32
GoIO_Sensor_DDSMem_SetLongName(hSensor, pLongName)
	GOIO_SENSOR_HANDLE	hSensor
	const char *	pLongName

gtype_int32
GoIO_Sensor_DDSMem_SetLotCode(hSensor, YY_BCD, WW_BCD)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	YY_BCD
	unsigned char	WW_BCD

gtype_int32
GoIO_Sensor_DDSMem_SetManufacturerID(hSensor, ManufacturerID)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	ManufacturerID

gtype_int32
GoIO_Sensor_DDSMem_SetMemMapVersion(hSensor, MemMapVersion)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	MemMapVersion

gtype_int32
GoIO_Sensor_DDSMem_SetMinSamplePeriod(hSensor, MinSamplePeriod)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_real32	MinSamplePeriod

gtype_int32
GoIO_Sensor_DDSMem_SetOperationType(hSensor, OperationType)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	OperationType

gtype_int32
GoIO_Sensor_DDSMem_SetRecord(hSensor, pRec)
	GOIO_SENSOR_HANDLE	hSensor
	const GSensorDDSRec *	pRec

gtype_int32
GoIO_Sensor_DDSMem_SetSensorNumber(hSensor, SensorNumber)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	SensorNumber

gtype_int32
GoIO_Sensor_DDSMem_SetSerialNumber(hSensor, leastSigByte_SerialNumber, midSigByte_SerialNumber, mostSigByte_SerialNumber)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	leastSigByte_SerialNumber
	unsigned char	midSigByte_SerialNumber
	unsigned char	mostSigByte_SerialNumber

gtype_int32
GoIO_Sensor_DDSMem_SetShortName(hSensor, pShortName)
	GOIO_SENSOR_HANDLE	hSensor
	const char *	pShortName

gtype_int32
GoIO_Sensor_DDSMem_SetSignificantFigures(hSensor, SignificantFigures)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	SignificantFigures

gtype_int32
GoIO_Sensor_DDSMem_SetTypNumberofSamples(hSensor, TypNumberofSamples)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_uint16	TypNumberofSamples

gtype_int32
GoIO_Sensor_DDSMem_SetTypSamplePeriod(hSensor, TypSamplePeriod)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_real32	TypSamplePeriod

gtype_int32
GoIO_Sensor_DDSMem_SetUncertainty(hSensor, Uncertainty)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	Uncertainty

gtype_int32
GoIO_Sensor_DDSMem_SetWarmUpTime(hSensor, WarmUpTime)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_uint16	WarmUpTime

gtype_int32
GoIO_Sensor_DDSMem_SetYmaxValue(hSensor, YmaxValue)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_real32	YmaxValue

gtype_int32
GoIO_Sensor_DDSMem_SetYminValue(hSensor, YminValue)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_real32	YminValue

gtype_int32
GoIO_Sensor_DDSMem_SetYscale(hSensor, Yscale)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	Yscale

gtype_int32
GoIO_Sensor_DDSMem_WriteRecord(hSensor, timeoutMs)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_int32	timeoutMs

gtype_int32
GoIO_Sensor_GetLatestRawMeasurement(hSensor)
	GOIO_SENSOR_HANDLE	hSensor

gtype_real64
GoIO_Sensor_GetMaximumMeasurementPeriod(hSensor)
	GOIO_SENSOR_HANDLE	hSensor

gtype_real64
GoIO_Sensor_GetMeasurementPeriod(hSensor, timeoutMs)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_int32	timeoutMs

gtype_real64
GoIO_Sensor_GetMeasurementTickInSeconds(hSensor)
	GOIO_SENSOR_HANDLE	hSensor

gtype_real64
GoIO_Sensor_GetMinimumMeasurementPeriod(hSensor)
	GOIO_SENSOR_HANDLE	hSensor

gtype_int32
GoIO_Sensor_GetNextResponse(hSensor, pRespBuf, pnRespBytes, pCmd, pErrRespFlag, nTimeoutMs)
	GOIO_SENSOR_HANDLE	hSensor
	void *	pRespBuf
	gtype_int32 *	pnRespBytes
	unsigned char *	pCmd
	gtype_int32 *	pErrRespFlag
	gtype_int32	nTimeoutMs



gtype_int32
GoIO_Sensor_GetProbeType(hSensor)
	GOIO_SENSOR_HANDLE	hSensor

gtype_int32
GoIO_Sensor_Lock(hSensor, timeoutMs)
	GOIO_SENSOR_HANDLE	hSensor
	gtype_int32	timeoutMs



gtype_int32
GoIO_Sensor_SendCmd(hSensor, cmd, pParams, nParamBytes)
	GOIO_SENSOR_HANDLE	hSensor
	unsigned char	cmd
	void *	pParams
	gtype_int32	nParamBytes

gtype_int32
GoIO_Sensor_SendCmdAndGetResponse(GOIO_SENSOR_HANDLE hSensor,  unsigned char  cmd)
    PREINIT:
	void *         pParams     = NULL;
	gtype_int32    nParamBytes = 0;
	void *         pRespBuf    = NULL;
	gtype_int32 *  pnRespBytes = NULL;
    CODE:
        RETVAL = GoIO_Sensor_SendCmdAndGetResponse(hSensor, cmd, pParams, nParamBytes, pRespBuf, pnRespBytes, SKIP_TIMEOUT_MS_DEFAULT);
    OUTPUT:
        RETVAL

gtype_int32
GoIO_Sensor_Unlock(hSensor)
	GOIO_SENSOR_HANDLE	hSensor

gtype_int32
GoIO_Uninit()

gtype_int32
GoIO_UpdateListOfAvailableDevices(vendorId, productId)
	gtype_int32	vendorId
	gtype_int32	productId
    OUTPUT:
	RETVAL

