package Vernier::GoTemp;

use base(Vernier::GoIO);

use Vernier::GoIO qw(:const);

sub new {

    my $class = shift;
    my %arg   = @_;

    my $self = $class->SUPER::new;

    foreach (keys %arg) {
        /^intervals$/ && do { $self->set_intervals($arg{intervals}); };
        /^callbacks$/ && do { $self->set_callbacks($arg{callbacks}); };
        /^duration$/  && do { $self->set_duration ($arg{duration} ); };
    }

    return $self;
}

sub set_intervals {

    my $self = shift;
    my $arg  = shift;

    # Measurement interval.
    $self->{measurement_interval} = Vernier::GoIO::set_measurement_interval(
            $arg->{measure} || $default_interval{measure}
    );

    # Read interval.
    $self->{read_interval} = Vernier::GoIO::set_read_interval(
            $arg->{read} || $default_interval{read}
    );

}

sub set_callbacks {

    my $self = shift;
    my $arg  = shift;

    $self->{read_callback} = $arg{read} || sub { @_ };
}

sub set_duration {

    my $self = shift;
    $self->{duration} = Vernier::GoIO::convert_to_seconds(shift);
}


sub open {

    my $self = shift;

    $self->{handle} = Vernier::GoIO::open_device(
        $self->{deviceID}, GoTemp(),
    );

    die "Unable to open GoTemp device!" unless defined $self->{handle};
    return $self;
}


sub close {
    my $self = shift;
    Vernier::GoIO::close_device($self->{handle});
}

sub start {
    my $self = shift;
    return Vernier::GoIO::start_collection($self->{handle});
}

sub read_measurements {
    my $self = shift;
    return Vernier::GoIO::read_measurements($self->{handle});

=for nobody

    $self->{read_callback}->(
        Vernier::GoIO::read_measurements($self->{handle})
    );
=cut
}

#
# The simplest possible event loop.
#
sub loop {

    my $self = shift;

    $SIG{ALRM} = sub { die "timeout" };

    if ($self->{duration}) {
        eval {
            alarm(5);
            #alarm($self->{duration});
            while (1) {
                $self->read_measurements;

            }
            alarm(0);
        };
        die "$@" if $@;
    }
}

sub DESTROY {
    my $self = shift;
    $self->close;
}

=head1 NAME

Vernier::GoTemp - Perl interface to the Go! Temp temperature probe

=head1 SYNOPSIS

use Vernier::GoTemp;

my $temp = Vernier::GoTemp->new(
    intervals => {
         measure => '.5s',   
         read    => '1s',   
    }
    callbacks => {
        read => sub { 
           my @measurements = @_;
           map { print "$_\n" } @measurements;
        },
        stop => 'stop_collection',
    }
);

$temp->start_collection;



=head1 DESCRIPTION

Vernier::GoTemp is a Perl interface to the Vernier Go! Temp temperature probe.  It provides methods to initialize the device, set measurement intervals, 
and read measurements.

=head1 METHODS

=head2 new()

=head2 open()

=head2 set_interval()

=head2 start()

=head2

=head2 close()


=head1 SEE ALSO

include/GoIO_DLL_interface.h - documents the GoIO C/C++ interface


=head1

=cut

1;
