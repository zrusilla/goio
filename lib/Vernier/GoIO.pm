package Vernier::GoIO;

use 5.008008;
use strict;
use warnings;
use Carp;

use Data::Dumper;
use Devel::Peek qw(Dump DumpArray);

require Exporter;
our @ISA = qw(Exporter);

our %EXPORT_TAGS = ();

# USB: Vendor ID and individual device IDs
my @goio_devices = qw(
    VernierID
    LabPro
    GoTemp
    GoLink
    GoMotion
    LabQuest
    CKSpectrometer
    MiniGasChromatograph
);


# Parameters to Sensor_SendCmdAndGetResponse().
my @goio_commands = qw(
    StartMeasurements
    StopMeasurements
    DefaultTimeout
);

# Simplified Perl procedural API
my @basic_api = qw(
   get_GoIO_version  
   get_available_device  
   open_device
   close_device
   get_min_period
   get_max_period
   set_measurement_interval
   set_read_interval
   get_tick
   get_device_info
);

# Perl interface to underlying GoIO functions

my @goio_api = qw(
    GetDLLVersion
    GetNthAvailableDeviceName
    Init
    Uninit
    UpdateListOfAvailableDevices
    Sensor_SendCmdAndGetResponse
    Sensor_GetMinimumMeasurementPeriod
    Sensor_GetMaximumMeasurementPeriod
);

$EXPORT_TAGS{const} = [
    @goio_devices,
    @goio_commands
]; 

$EXPORT_TAGS{basic} = [
    @goio_devices,
    @basic_api
];

$EXPORT_TAGS{goio_api} = [ 
    @goio_devices,
    @goio_commands,
    @goio_api
];

$EXPORT_TAGS{all} = [ 
    @goio_devices,
    @goio_commands,
    @basic_api,
    @goio_api,
];

our @EXPORT_OK = qw(
    const
    basic
    goio_api
    all
);

push @EXPORT_OK, @{$EXPORT_TAGS{all}};

my %device = (
    'LabPro'               => \&LabPro,
    'GoTemp'               => \&GoTemp,
    'GoLink'               => \&GoLink,
    'GoMotion            ' => \&GoMotion,
    'LabQuest'             => \&LabQuest,
    'CKS'                  => \&CKSpectrometer,
    'CKSpectrometer'       => \&CKSpectrometer,
    'MGS'                  => \&MiniGasChromatograph,
    'MiniGasChromatograph' => \&MiniGasChromatograph,
);


my %default_interval = (
    measure => '50ms',
    read    => '1s',
);

BEGIN {
    our $VERSION = '0.01';
    require XSLoader;
    XSLoader::load('Vernier::GoIO', $VERSION);
    Init();
}


# For the object interface.

sub new {
    my $class = shift;
    my %arg   = @_;

    my $device = (split /::/, $class)[1];

    my $self = {};

    # Open the device.

    $device{$device} ?
        $self->{deviceID} = $device{$device} : 
        return undef
    ;
        
    $self->{device} = get_available_device($device);

    $self->{handle} = open_device(
        $self->{device},
        $self->{deviceID},
    );

    die "Can't open $device device!" unless $self->{handle};

    return bless $self, $class;
}

sub set_callback {
}

sub set_duration {
}

sub get_GoIO_version {

    my ($major, $minor) =  GetDLLVersion();
    return "$major.$minor";
}

sub get_available_device {

    my ($self, $arg);

    ref $_[0]  ? 
       ($self, $arg) = @_ :
       $arg          = shift
    ;

    die 'No device specified' unless $arg;

    my $device_id = 

        $self          ? $self->{deviceID}:    # cached deviced ID
        $arg =~ /\d+/  ? $arg             :    # device ID
        $device{$arg}  ? $device{$arg}()  :    # device name
        die 'Invalid device specified' 
    ;

    my $num  = UpdateListOfAvailableDevices(VernierID(), $device_id);
    my $name = GetNthAvailableDeviceName(   VernierID(), $device_id);

    return $name;
}

sub open_device {

    my $device_name = shift;
    my $device_id  = shift;

    my $id = ref $device_id ?   $device_id->() : $device_id;

    my $handle = Sensor_Open($device_name, VernierID(), $id, 0);

    die "Unable to open device!" unless defined $handle;

    return $handle;

}

sub close_device {
    my $handle =  shift;

    my $retval = Sensor_Close($handle);

    die "Unable to close device!" if $retval != 0;
}

sub get_max_period {
    my $handle = shift;
    return Sensor_GetMaximumMeasurementPeriod($handle);
}

sub get_min_period {
    my $handle = shift;
    return Sensor_GetMinimumMeasurementPeriod($handle);
}

sub set_measurement_interval {

    my $handle = shift;
    my $interval = shift;

    my $time = convert_to_seconds($interval);

    die 'No interval specified!'  unless $time;

    die "Interval $time not supported by device" 
       unless  $time <= Sensor_GetMaximumMeasurementPeriod($handle) 
            && $time >= Sensor_GetMinimumMeasurementPeriod($handle) 
    ;

    Sensor_SetMeasurementPeriod($handle, $time);

    return Sensor_GetMeasurementPeriod($handle, DefaultTimeout);
}


sub set_read_interval {
    return  convert_to_seconds(shift);
}

# Turn a time string into seconds.

sub convert_to_seconds {
    my $interval = shift;

    my ($number, $unit) =  $interval =~ /(\d+)([ms]{1,2})/;

    return
        $unit eq 's'  ?  $number          :
        $unit eq 'm'  ?  $number  * 60    :
        $unit eq 'ms' ?  $number  * .001  : 0
   ;
}

sub get_tick {

    my $handle = shift;

    die "No sensor handle!" unless defined $handle && $handle;

    return Sensor_GetMeasurementTickInSeconds($handle);
}

sub get_device_info {

    my $handle = shift;
    
    my @info = Sensor_GetOpenDeviceName($handle);

    die "No device info found for $handle!" unless @info;

    return ( 
        'name'       =>  $info[0],
        'vendor_id'  =>  $info[1],
        'product_id' =>  $info[2],
    );

}

sub start_collection {

    my $handle = shift;
    my %arg    = @_;

    die "No sensor handle!" unless defined $handle && $handle;

    my $retval = Sensor_ClearIO($handle);

    die "Could not clear device buffer!" unless $retval == 0;

    $retval = Sensor_SendCmdAndGetResponse($handle, StartMeasurements); 

    die "Could not start measurements!" unless $retval == 0;
}

sub stop_collection {

    my $handle = shift;
    my %arg    = @_;

    die "No sensor handle!" unless defined $handle && $handle;

    my $retval = Sensor_SendCmdAndGetResponse(
        $handle,
        StopMeasurements,
        undef, 0, undef, undef, 
        DefaultTimeout
    ); 

    die "Could not stop measurements!" unless $retval == 0;
}

sub read_measurements {
    my $handle = shift;

    die "No sensor handle!" unless defined $handle && $handle;

    my @measurements;

    my $available = Sensor_GetNumMeasurementsAvailable($handle);

    if ($available > 0) {

        my @raw = Sensor_ReadRawMeasurements($handle, $available);
  
        if (@raw > 0) {

            foreach my $raw (@raw) {
                my $volts = Sensor_ConvertToVoltage($handle, $raw);
                my $value = Sensor_CalibrateData($handle, $volts);

                push @measurements, $value; 
            }
        }
    }
    return @measurements;
}


1;
__END__

=head1 NAME

Vernier::GoIO - Perl interface to the GoIO SDK.

Also serves as a base class for Vernier Go! device-specific classes.

=head1 SYNOPSIS

  Driver subclass:

      package Vernier::GoDevice;
      use base (Vernier::GoIO);

=head1 DESCRIPTION

=head2 EVENT LOOPS

Vernier GoIO devices have rough measurement granularity (Go! Temp's minimum interval is just under .5 seconds)
so an event loop using Perl's sleep()  and alarm () functions should be adequate.

=head2 EXPORT

Constants
    VernierID
    LabPro
    GoTemp
    GoLink
    GoMotion
    LabQuest
    CKSpectrometer
    MiniGasChromatograph


Subroutines
    GetDLLVersion
    Init
    UpdateListOfAvailableDevices
    GetNthAvailableDeviceName

=head1 SEE ALSO


=head1 AUTHOR

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009 by zrusilla

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.8 or,
at your option, any later version of Perl 5 you may have available.


=cut


1;
