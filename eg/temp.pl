use Vernier::GoTemp;

my $temp = Vernier::GoTemp->new(
    intervals => {
         measure => '.5s',
         read    => '1s',
    }
    callbacks => {
        read    => sub {
           my @measurements = @_;
           map { print "$_\n" } @measurements;
        }
    }
);


print $temp->get_GoIO_version, "\n";

$temp->start_collection;

sleep(300);

$temp->stop_collection;

